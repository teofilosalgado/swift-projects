//
//  GameControllerViewController.swift
//  App
//
//  Created by aluno on 09/10/18.
//  Copyright © 2018 sigmadigital. All rights reserved.
//

import UIKit

class GameController: UIViewController {

    private var _currentPlayer: Bool = false
    var matrix = [[Int]](repeating: [Int](repeating: 0, count: 3), count: 3)
    
    var currentPlayer: Bool{
        get{
            return _currentPlayer
        }
        set(newValue){
            _currentPlayer = newValue
            if(newValue == true){
                playerDisplay.text? = "VEZ DO JOGADOR 2"
            }
            else{
                playerDisplay.text? = "VEZ DO JOGADOR 1"
            }
        }
    }
    
    @IBOutlet weak var playerDisplay: UILabel!
    @IBOutlet weak var button1o: UIButton!
    @IBOutlet weak var button2o: UIButton!
    @IBOutlet weak var button3o: UIButton!
    @IBOutlet weak var button4o: UIButton!
    @IBOutlet weak var button5o: UIButton!
    @IBOutlet weak var button6o: UIButton!
    @IBOutlet weak var button7o: UIButton!
    @IBOutlet weak var button8o: UIButton!
    @IBOutlet weak var button9o: UIButton!
    
    
    @IBAction func button1a(_ sender: Any) {
        if(currentPlayer){
            matrix[0][0] = 2
        }
        else{
            matrix[0][0] = 1
        }
        buttonPressed(sender: button1o)
    }
    @IBAction func button2a(_ sender: Any) {
        if(currentPlayer){
            matrix[0][1] = 2
        }
        else{
            matrix[0][1] = 1
        }
        buttonPressed(sender: button2o)
    }
    @IBAction func button3a(_ sender: Any) {
        if(currentPlayer){
            matrix[0][2] = 2
        }
        else{
            matrix[0][2] = 1
        }
        buttonPressed(sender: button3o)
    }
    @IBAction func button4a(_ sender: Any) {
        if(currentPlayer){
            matrix[1][0] = 2
        }
        else{
            matrix[1][0] = 1
        }
        buttonPressed(sender: button4o)
    }
    @IBAction func button5a(_ sender: Any) {
        if(currentPlayer){
            matrix[1][1] = 2
        }
        else{
            matrix[1][1] = 1
        }
        buttonPressed(sender: button5o)
    }
    @IBAction func button6a(_ sender: Any) {
        if(currentPlayer){
            matrix[1][2] = 2
        }
        else{
            matrix[1][2] = 1
        }
        buttonPressed(sender: button6o)
    }
    @IBAction func button7a(_ sender: Any) {
        if(currentPlayer){
            matrix[2][0] = 2
        }
        else{
            matrix[2][0] = 1
        }
        buttonPressed(sender: button7o)
    }
    @IBAction func button8a(_ sender: Any) {
        if(currentPlayer){
            matrix[2][1] = 2
        }
        else{
            matrix[2][1] = 1
        }
        buttonPressed(sender: button8o)
    }
    @IBAction func button9a(_ sender: Any) {
        if(currentPlayer){
            matrix[2][2] = 2
        }
        else{
            matrix[2][2] = 1
        }
        buttonPressed(sender: button9o)
    }
    
    
    func buttonPressed(sender: UIButton)
    {
        if(sender.titleLabel?.text == "?"){
            if(!currentPlayer){
                sender.setTitle("X", for: [])
            }
            else{
                sender.setTitle("O", for: [])
            }
            currentPlayer = !currentPlayer
        }
         
        verifyWinner()
    }
    
    func verifyWinner()
    {
        if([matrix[0][0], matrix[1][1], matrix[2][2]].elementsEqual([1,1,1]))
        {
            callWinnerScreen(player: 1)
        }
        if([matrix[0][0], matrix[1][1], matrix[2][2]].elementsEqual([2,2,2]))
        {
            callWinnerScreen(player: 2)
        }
        
        if([matrix[2][0], matrix[1][1], matrix[0][2]].elementsEqual([1,1,1]))
        {
            callWinnerScreen(player: 1)
        }
        if([matrix[2][0], matrix[1][1], matrix[0][2]].elementsEqual([2,2,2]))
        {
            callWinnerScreen(player: 2)
        }
        
        if([matrix[0][0], matrix[1][0], matrix[2][0]].elementsEqual([1,1,1]))
        {
            callWinnerScreen(player: 1)
        }
        if([matrix[0][0], matrix[1][0], matrix[2][0]].elementsEqual([2,2,2]))
        {
            callWinnerScreen(player: 2)
        }
        if([matrix[0][1], matrix[1][1], matrix[2][1]].elementsEqual([1,1,1]))
        {
            callWinnerScreen(player: 1)
        }
        if([matrix[0][1], matrix[1][1], matrix[2][1]].elementsEqual([2,2,2]))
        {
            callWinnerScreen(player: 2)
        }
        if([matrix[0][2], matrix[1][2], matrix[2][2]].elementsEqual([1,1,1]))
        {
            callWinnerScreen(player: 1)
        }
        if([matrix[0][2], matrix[1][2], matrix[2][2]].elementsEqual([2,2,2]))
        {
            callWinnerScreen(player: 2)
        }
        
        if([matrix[0][0], matrix[0][1], matrix[0][2]].elementsEqual([1,1,1]))
        {
            callWinnerScreen(player: 1)
        }
        if([matrix[0][0], matrix[0][1], matrix[0][2]].elementsEqual([2,2,2]))
        {
            callWinnerScreen(player: 2)
        }
        if([matrix[1][0], matrix[1][1], matrix[1][2]].elementsEqual([1,1,1]))
        {
            callWinnerScreen(player: 1)
        }
        if([matrix[1][0], matrix[1][1], matrix[1][2]].elementsEqual([2,2,2]))
        {
            callWinnerScreen(player: 2)
        }
        if([matrix[2][0], matrix[2][1], matrix[2][2]].elementsEqual([1,1,1]))
        {
            callWinnerScreen(player: 1)
        }
        if([matrix[2][0], matrix[2][1], matrix[2][2]].elementsEqual([2,2,2]))
        {
            callWinnerScreen(player: 2)
        }
    }
    
    func callWinnerScreen(player: Int)
    {
        self.performSegue(withIdentifier: "WinnerSegue", sender:nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
